import logging
import os, sys
sys.path.append(os.getcwd())


class log:
	logpeewee = logging.getLogger('peewee')
	logpeewee.setLevel(logging.DEBUG)
	file_log_handler = logging.FileHandler('vocatus.log')
	logpeewee.addHandler(file_log_handler)

	stderr_log_handler = logging.StreamHandler()
	logpeewee.addHandler(stderr_log_handler)

	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	file_log_handler.setFormatter(formatter)
	stderr_log_handler.setFormatter(formatter)

	#############################################

	log = logging.getLogger('vocatus')
	log.setLevel(logging.DEBUG)
	file_log_handler = logging.FileHandler('vocatus.log')
	log.addHandler(file_log_handler)

	stderr_log_handler = logging.StreamHandler()
	log.addHandler(stderr_log_handler)

	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	file_log_handler.setFormatter(formatter)
	stderr_log_handler.setFormatter(formatter)
	log.info("Iniciando Vocatus Versão 1.0")