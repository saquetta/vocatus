'''
Created on 22 de nov de 2017

@author: gustavosaquetta
'''
import os, sys
sys.path.append(os.getcwd())

import gspread
import file_encryptor.convergence
from oauth2client.service_account import ServiceAccountCredentials
from uuid import getnode as get_mac
import base64
from Crypto.Cipher import AES
import json

file = '/Users/gustavosaquetta/vocatus/src/controller/lib/drmssqt.json'
xfile = '/Users/gustavosaquetta/vocatus/src/controller/lib/drmssqtx.json'

def ssqtcrypto(arquivo):
    chave = "0123456789ABCDEF"
    aes = AES.new(chave, AES.MODE_ECB)

    arq_entrada = open(arquivo, "r")
    arq_entrada = arq_entrada.read()

    cryptoSaida = arq_entrada+'#'*(16-len(arq_entrada)%16)

    texto_cifrado = base64.b32encode(aes.encrypt(cryptoSaida))

    titulo_novo=base64.b32encode(aes.encrypt(arquivo+'#'*(16-len(arquivo)%16)))

    arq_saida = open(arquivo,'wb')
    arq_saida.write(texto_cifrado)
    arq_saida.close()

def ssqtdecrypto(arquivo):
    chave = "0123456789ABCDEF"

    try:
        aes = AES.new(chave, AES.MODE_ECB)
        arq_entrada = open(arquivo, "r")
        arq_entrada = base64.b32decode(arq_entrada.read())

        data = eval(aes.decrypt(arq_entrada))

        if data:
            with open(arquivo, 'w') as outfile:
                json.dump(data, outfile)
    except:
        pass


ssqtdecrypto(file)
# conn
class APIGoogleSheet:
    def __init__(self):
        self.page = None
        self.get_page()

    def connect(self):
        try:
            scope = ['https://spreadsheets.google.com/feeds']
            creds = ServiceAccountCredentials.from_json_keyfile_name(file, scope)
            ssqtcrypto(file)
            client = gspread.authorize(creds)
            return client
        except:
            pass

    def get_page(self, page=1):
        try:
            client = self.connect()
            sheet = client.open("drm")
            self.page  = sheet.get_worksheet(page)
            return page
        except:
            pass

    def get_all_data(self):
        try:
            page = self.page
            all_data = page.get_all_records()
            return all_data
        except:
            pass

    def set_row_data(self, data_list=[]):
        try:
            if not data_list:
                return

            page = self.page
            page.insert_row(data_list, self.get_next_row())
            return data_list
        except:
            pass

    def get_next_row(self):
        try:
            page = self.page
            next_value = len(page.get_all_values())+1
            return next_value
        except:
            pass

    def update_row_data(self, data_list, id):
        try:
            data = self.get_all_data()
            index = 2
            key = [x for x in data if x['id'] == id]

            if key:
                for row in data:
                    if row['id']==id:
                        break
                    index += 1

            page = self.page
            i = 1

            for data_value in data_list:
                page.update_cell(index, i, data_value)
                i += 1


        except:
            pass





AGS = APIGoogleSheet()
AGS.update_row_data(['saquetta',91696391,5,4,6], 4963564963)
