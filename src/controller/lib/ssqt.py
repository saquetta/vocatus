import os, sys, datetime, psutil
from PyQt5.QtWidgets import *
from datetime import date
from PyQt5.QtCore import QDate

#COSNTANTES
TYPE_OBJ_LIST_TEXT = [
						"<class 'PyQt5.QtWidgets.QLineEdit'>",
						"<class 'PyQt5.QtWidgets.QSpinBox'>",
						"<class 'PyQt5.QtWidgets.QDoubleSpinBox'>",
						"<class 'PyQt5.QtWidgets.QDateEdit'>",
						"<class 'PyQt5.QtWidgets.QTimeEdit'>",
					]

TYPE_OBJ_LIST_TEXT_PLAIN = ["<class 'PyQt5.QtWidgets.QTextEdit'>",]

TYPE_OBJ_LIST_COMBO = ["<class 'PyQt5.QtWidgets.QComboBox'>",]

TYPE_OBJ_LIST_LOOKUP = ["<class 'PyQt5.QtWidgets.QGroupBox'>",
						"<class 'PyQt5.QtWidgets.QLineEdit'>",
						"<class 'PyQt5.QtWidgets.QSpinBox'>",
					   ]
				
TYPE_OBJ_LIST_IS_CHECKED = [
								"<class 'PyQt5.QtWidgets.QCheckBox'>",
								"<class 'PyQt5.QtWidgets.QRadioButton'>",
						   ]
						   
ALL_OBJ_LIST = [] + TYPE_OBJ_LIST_TEXT + TYPE_OBJ_LIST_COMBO + TYPE_OBJ_LIST_IS_CHECKED + TYPE_OBJ_LIST_TEXT_PLAIN+TYPE_OBJ_LIST_LOOKUP

class SSQtSystem:
	def pdb(self):
		'''Tive que utilizar no MAC'''
		from PyQt5.QtCore import pyqtRemoveInputHook
		# não remova o PDB PORRA!!!!
		from pdb import set_trace
		pyqtRemoveInputHook()
		
		# Musiquinha do Matriz
		set_trace()

	def quitter(self, including_parent=False):
		'''
			Chupa essa filho da puta!!!
			Saporra do demonio!!!
			Utilizar com sabedoria, é feito para matar cursor do banco que não morre diabo!!!
			Matar geral quando tiver trace interno algo assim!
		'''
		parent = psutil.Process(os.getpid())

		for child in parent.children(recursive=True):
			child.kill()

		if including_parent:
			parent.kill()

class SSQt(SSQtSystem):

	def _get_ui_objects(self):
		"""
			Objetivo: Capturar e retornar os objetos da tela.
			Retorno: Objeto e tipo do objeto.
		"""
		obj_dict_list = []
		
		for obj in dir(self):
			if str(type(getattr(self,obj))) in ALL_OBJ_LIST:

				value = str(type(getattr(self,obj)))
				obj_dict_list.append({obj:value})

		return obj_dict_list

	def ui(self):
		# Definir valor padrão para tdate e ttime
		obj_dict_list = self._get_ui_objects()
		for obj_dict in obj_dict_list:
			for k, v in obj_dict.items():
				if v == TYPE_OBJ_LIST_TEXT[3]:
					now = date.today()
					today = QDate(int(now.year), int(now.month), int(now.day))
					getattr(self,k).setDate(today)
				#define valor padrao

				#if getattr(self,k).whatsThis() == 'int':
				#	self.onlyInt = QIntValidator()
				#	getattr(self,k).setValidator(self.onlyInt)

				#elif getattr(self,k).whatsThis() == 'bool':
				#	getattr(self,k).setValidator(self.onlyInt)
					
				#elif getattr(self,k).whatsThis() == 'float':
				#	self.onlyDouble = QDoubleValidator()
				#	getattr(self,k).setValidator(self.onlyDouble)

	def get_ui_dict(self, format=True):
		"""
			Objetivo: Processar o tipo de dado do objeto da tela e retornar o valor armazenado.
			Retorno: Dicionario com os objetos da tela.
		"""
		obj_dict_list = self._get_ui_objects()
		win_dict = {}
		rb_dict = {}

		for obj_dict in obj_dict_list:
			value = None
			for k, v in obj_dict.items():
				#Validacao para tipo de Texto
				if v in TYPE_OBJ_LIST_TEXT:
					value = getattr(self,k).text()
					win_dict.update({k:value})

				if v in TYPE_OBJ_LIST_TEXT_PLAIN:
					value = getattr(self,k).toPlainText()
					win_dict.update({k:value})

				if v in TYPE_OBJ_LIST_IS_CHECKED:
					check = getattr(self,k).isChecked()

					if v == TYPE_OBJ_LIST_IS_CHECKED[0]:
						win_dict.update({k:check})

					if v == TYPE_OBJ_LIST_IS_CHECKED[1] and getattr(self,k).isChecked() == True:
						rb_dict.update({k:check})
					
				if v in TYPE_OBJ_LIST_COMBO:
					# lista index e nome da lista
					#text = getattr(self,k).currentText()
					index = getattr(self,k).currentIndex()
					win_dict.update({k:index})

				if v in TYPE_OBJ_LIST_LOOKUP:
					if getattr(self,k).whatsThis().lower().startswith('lookup'):
						if v in TYPE_OBJ_LIST_LOOKUP[0]:
							value = getattr(self,k).property('id')
							value_text = getattr(self,k).property('title')
							if not value:
								print('Não informado o ID para lookup!')
							win_dict.update({k:value})
							k = k+'_title'
							win_dict.update({k:value_text})

			# Formato de acordo com o tipo de dados.
			if (format and value) and k in dir(self):
				if getattr(self,k).whatsThis() == 'str':
					win_dict.update({k:str(value)})
				elif getattr(self,k).whatsThis() == 'int':
					win_dict.update({k:int(value)})
				elif getattr(self,k).whatsThis() == 'bool':
					win_dict.update({k:int(check)})
				elif getattr(self,k).whatsThis() == 'float':
					win_dict.update({k:self.SSQtFloat(value)})
				elif getattr(self,k).whatsThis() == 'date':
					value =value.replace('/','-')

					dia, mes, ano = value.split('-')
					value = '-'.join([ano, mes, dia])
					win_dict.update({k:value})

				elif getattr(self,k).whatsThis() == 'time':
					win_dict.update({k:value})
				elif getattr(self,k).whatsThis() == 'cb:int':
					win_dict.update({k:int(index)})
				elif getattr(self,k).whatsThis().lower().startswith('lookup') and 'int' in getattr(self,k).whatsThis().split(':'):
					win_dict.update({k:int(value)})

		# Valida todos grupos de radio button.
		# Escolhe comente o marcado e utiliza a chave e o valor para gravar no banco.

		for k, v in rb_dict.items():
			rb = k.split('_')
			win_dict.update({rb[0]:int(rb[1])})

		return win_dict

	def insert_item_tree(self, tree_name, title_list, row_column_dict={}, checkable=False):
		"""
			Objetivo: Inserir dados no tree_view.
		"""
		insert_dict = {}
		iter = 0
		item=QTreeWidgetItem()
		if not insert_dict:
			insert_dict = self.get_ui_dict(format = False)
			
		#if not [x for x in list(insert_dict.values()) if x =='']:
		#	return
		
		if not row_column_dict:
			row_column_dict = insert_dict
			row_column_dict.update({'all_form':'all_form'})
		
		posicao = 0
		for title in title_list:
			#import pdb;pdb.set_trace()
			getattr(self, tree_name).setColumnWidth(posicao,title[2])
			posicao += 1
			if title[0] != 'all_form':
				# Tree só aceita tipos string

				value = row_column_dict.get(title[1]) or ''
				item.setText(iter, str(value))

			if title[0] == 'all_form':
				if insert_dict:
					str_ui = str(insert_dict)
					item.setText(iter, str_ui)
				if row_column_dict:
					item.setText(iter, str(row_column_dict))

			getattr(self, tree_name).addTopLevelItem(item)
			iter += 1

	def get_selected_row(self, tree_name, title_list):
		"""
			Objetivo: Processar alinha clicada duas vezes.
			Retorno: Dicionario com os dados da linha.
		"""
		count = 0
		row_dict = {}
		aux_dict = {}
		var = None

		for title in title_list:
			try:
				value = getattr(self, tree_name).currentItem().text(count)
			except: 
				return  {}

			if title[0] != 'all_form':
				#row_dict.update({map_dict[title[0]]:value})
				row_dict.update({title[1]:value})

			else:
				#TODO é mais não é bem assim
				if value.find('datetime') != -1:
					posicao = value[value.find('datetime'):value.find('datetime')+27]

					var = eval(posicao)
					if hasattr(var, 'count'):
						date = "'%s' %s"  % (str(var[0]), ',')
					else:
						date = str(var)

					#ano, mes, dia = value[value.find('datetime'):value.find('datetime')+27][14:-1].split(',')
					#self.pdb()
					#var = ('-').join([dia[1:], mes[1:], ano])
					value = value.replace(value[value.find('datetime'):value.find('datetime')+27], date)

				dict_complementar = eval(value)
				dict_complementar_type = type(eval(value))
				
				if dict_complementar_type == dict:
					row_dict['all_form'] = dict_complementar

			count += 1
		#nem
		#row_dict.update(aux_dict)
		#return row_dict
		return dict_complementar
		
	def get_data_tree(self, tree_name, title_list):
		"""
			Objetivo: Processar alinha clicada duas vezes.
			Retorno: Dicionario com os dados da linha.
		"""
		rows = getattr(self, tree_name).topLevelItemCount()
		data_dict_list = []

		if not rows:
			return data_dict_list

		for row in range(0, rows):
			count = 0
			row_dict = {}
	
			for title in title_list:			
				value = getattr(self, tree_name).topLevelItem(row).text(count)
				row_dict.update({title[1]:value})
				count += 1

			data_dict_list.append(row_dict)
		ret = [eval(x['all_form']) for x in data_dict_list]

		return ret
		
	def remove_item_tree(self, tree_name):
		"""
			Remove item do tree_view.
		"""
		getattr(self, tree_name).takeTopLevelItem(getattr(self, tree_name).currentIndex().row())

	def clrscr(self):
		"""
			Limpa a tela.
			RED-DEMON says: Sempre quis ter um método com este nome executando essa funcionalidade.
		"""
		obj_dict_list = self._get_ui_objects()

		for obj in dir(self):
			if str(type(getattr(self,obj))) in TYPE_OBJ_LIST_TEXT:
				getattr(self,obj).clear()

		for obj_dict in obj_dict_list:
			for k, v in obj_dict.items():
				if v in TYPE_OBJ_LIST_LOOKUP:
					if getattr(self, k).whatsThis().lower().startswith('lookup'):
						if v in TYPE_OBJ_LIST_LOOKUP[0]:
							getattr(self,k).setProperty('id', '')
							getattr(self,k).setProperty('title', '')

		self.ui()

	def alter_item_tree(self, tree_name, title_list, ui_dict={}):
		"""
			Objetivo: Alterar um item no tree_view.
		"""
		count = 0
		row = getattr(self, tree_name).currentIndex().row()
		new_dict = {}

		if not ui_dict:
			ui_dict = self.get_ui_dict(format = False)

		if not [x for x in list(self.get_ui_dict(format = False).values()) if x !='']:
			return
		
		try:
			for title in title_list:
				key = title[1]
				if key != 'all_form':
					value = getattr(self, tree_name).currentItem().setData(count,0,ui_dict[key]) #linha,coluna,obj

				elif key == 'all_form' and type(eval(getattr(self, tree_name).currentItem().text(count))) is dict: #para o all_form
					#item_dict = eval(getattr(self, tree_name).currentItem().text(count))
					#item_dict.update(ui_dict)
					value = str(ui_dict)

					getattr(self, tree_name).currentItem().setData(count,0,value)

				count += 1

		except:
			print('Valores nulos para alterar um linha: ', key)

	def set_ui(self, dict):
		"""
			Objetivo: Definir valores em tela.
		"""
		if not dict or not [x for x in list(dict.values()) if x != '']:
			return
		self.clrscr()
		obj_dict_list = self._get_ui_objects()
		lookup_list = []
		field_list = []
		lookup_list_name = []
		dict.update(dict.get('all_form', {}))
		for obj_dict in obj_dict_list:
			for k, v in obj_dict.items():
				try:
					#Validacao para tipo de text
					if v in TYPE_OBJ_LIST_TEXT:

						if v == TYPE_OBJ_LIST_TEXT[0] or v in TYPE_OBJ_LIST_TEXT_PLAIN:
							getattr(self,k).setText(dict[k])
							
						if v == TYPE_OBJ_LIST_TEXT[1]:
							getattr(self,k).setValue(int(dict[k]))

						elif v == TYPE_OBJ_LIST_TEXT[2]:
							getattr(self,k).setValue(self.SSQtFloat(dict[k]))

						elif v == TYPE_OBJ_LIST_TEXT[3]:
							getattr(self,k).setDate(self.SSQtDate(dict[k]))
							
						elif v == TYPE_OBJ_LIST_TEXT[4]:
							getattr(self,k).setTime(self.SSQtTime(dict[k]))

					#Validacao para boolean (isChecked)
					
					elif v in TYPE_OBJ_LIST_IS_CHECKED:
						if v == TYPE_OBJ_LIST_IS_CHECKED[0]:
							getattr(self,k).setChecked(int(dict[k]))
							
						if v == TYPE_OBJ_LIST_IS_CHECKED[1]:
							
							rb = k.split('_')
							chave = '_'.join([rb[0],dict[rb[0]]])
							getattr(self,chave).setChecked(1)
							
							#toggled = getattr(self,k).objectName().endswith('')
							#if getattr(self,k).objectName().endswith(''):
								
							#.setChecked(int(dict[k]))

					elif v in TYPE_OBJ_LIST_COMBO:
						if type(dict[k]) == list:
							getattr(self,k).setCurrentIndex(int(dict[k][0]))
						#if type(v) == int or int(v) == int:
						getattr(self,k).setCurrentIndex(int(dict[k]))

					if v in TYPE_OBJ_LIST_LOOKUP:
						if getattr(self, k).whatsThis().lower().startswith('lookup'):
							if v in TYPE_OBJ_LIST_LOOKUP[0]:
								#import pdb;pdb.set_trace()
								getattr(self,k).setProperty('id', dict[k])
								#essa linha abaixo me fez perder umas 4 horas
								if dict.get(k+'_title'):
									getattr(self,k).setProperty('title', dict[k+'_title'])

								lookup_list.append(getattr(self,k))
								lookup_list_name.append(k)

				except:
					print ('Verificar se houve algum problema com o objetos catalogado.: ', k, v)
		
		for lookup in lookup_list:
			if lookup.property('id'):

				for name in lookup_list_name:
					if name != 'groupBoxLookup':
						getattr(self, name+'_codigo').setFocus()
						getattr(self, name+'_codigo').clearFocus()

	###############################################################################
	# criar uma classe para conversão de tipos
	def SSQtDate(self, str):
		#dia, mes, ano = str.split('/')
		dia, mes, ano = str.split('-')
		return QDate(int(ano), int(mes), int(dia))
		
	def SSQtTime(self, str):
		from PyQt5.QtCore import QTime
		if len(str) == 5:
			horas, minutos = str.split(':')
			return QTime(int(horas), int(minutos))
		else:
			horas, minutos, segundos = str.split(':')
			return QTime(int(horas), int(minutos), int(segundos))

	def SSQtFloat(self, str):
		value = float(str.replace(',','.'))
		return value

	def load_uifile(self, file):
		#if os.environ.get('DEVEL'):
		if os.environ.get('compileUi'):
			py = file.replace('.ui', '.py')
			script = 'pyuic5 %s -o %s' % (file, py)
			#import pdb;pdb.set_trace()
			if os.system(script):
				print(script)
				return True
			else:
				print(script)
				return False
		return True

	def validate_obrigatory_field(self):
		sender = self.sender()

		if sender.text() not in ('0', '0.0', '0.00', '0.000', ''):
			color = ''
		else:
			color = '#f6989d' # red meio gay

		if str(type(sender)) == TYPE_OBJ_LIST_TEXT[0]:
			sender.setStyleSheet('QLineEdit { background-color: %s }' % color)
		elif str(type(sender)) == TYPE_OBJ_LIST_TEXT[1]:
			sender.setStyleSheet('QSpinBox { background-color: %s }' % color)
		if str(type(sender)) == TYPE_OBJ_LIST_TEXT[2]:
			sender.setStyleSheet('QDoubleSpinBox { background-color: %s }' % color)